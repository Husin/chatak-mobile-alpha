package com.unpas.myapplication.main_page;


import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.unpas.myapplication.profile_page.ProfileFragment;
import com.unpas.myapplication.R;
import com.unpas.myapplication.transaksi_page.SignInActivity;


public class MainActivity extends AppCompatActivity {

    private Fragment fragment;
    private FragmentManager fragmentManager;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment =new HomeFragment();
                    break;
                case R.id.navigation_dokumen:
                    fragment =new DokumenFragment();
                    break;
                case R.id.navigation_media_promosi:
                    fragment =new MediaPromosiFragment();
                    break;
                case R.id.navigation_souvenir:
                    fragment =new SouvenirFragment();
                    break;
                case R.id.navigation_keranjang:
                    try {
                        fragment =new KeranjangFragment();
                        startActivity(new Intent(MainActivity.this, SignInActivity.class));
                        break;
                    }catch (Exception e){
                        Log.e("error",e.getMessage());
                    }

            }
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.container, fragment).commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//        center title
//        set title bar centered text
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayShowTitleEnabled(true);
//        actionBar.setTitle("Canteen Home");
//        actionBar.setHomeButtonEnabled(true);
//        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        actionBar.setCustomView(R.layout.abs_layout);
//        end of set title bar centered text

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            //sub menu selected
            case R.id.itemSetting:
                Toast.makeText(MainActivity.this,"Penggaturan clicked : no fragment is set",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemHelp:
                Toast.makeText(MainActivity.this,"Bantuan clicked : no fragment is set",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemAbout:
                Toast.makeText(MainActivity.this,"Tentang clicked : no fragment is set",Toast.LENGTH_SHORT).show();
                return true;
                //menu selected
            case R.id.itemAccount:
                try {
                    fragment =new ProfileFragment();
                    startActivity(new Intent(MainActivity.this,SignInActivity.class));
                    break;
                }catch (Exception e){
                    Log.e("error",e.getMessage());
                }
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

}

package com.unpas.myapplication.main_page;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unpas.myapplication.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MediaPromosiFragment extends Fragment {


    public MediaPromosiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_media_promosi, container, false);
    }

}

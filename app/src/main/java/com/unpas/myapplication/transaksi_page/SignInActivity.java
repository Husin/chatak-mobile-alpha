package com.unpas.myapplication.transaksi_page;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.unpas.myapplication.R;
import com.unpas.myapplication.main_page.MainActivity;
import com.unpas.myapplication.pojo.User;

public class SignInActivity extends AppCompatActivity {
    User user = new User();
    private EditText inEmail,inPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        findViewById(R.id.imageButtonBackSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignInActivity.super.onBackPressed();
            }
        });
        findViewById(R.id.textViewSignUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignInActivity.this,SignUpActivity.class));
            }
        });
        //assign edit text
        inEmail= findViewById(R.id.editTextEmail_sign_in);
        inPass=findViewById(R.id.editTextPassword_sign_in);
        //set dummy data
        user.setuEmail("admin@mail.com");
        user.setuPassword("admin");

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String sInEmail=inEmail.getText().toString();
                final String sInPass=inPass.getText().toString();
                authLogin(sInEmail,sInPass);
            }
        });
    }
    private void authLogin(String inEmail,String inPass){
        try {
            if (inEmail.equals(user.getuEmail())
                    &&inPass.equals(user.getuPassword()))
            {
                startActivity(new Intent(this, MainActivity.class));
            }else {
                Toast.makeText(getApplicationContext(),"email atau password tidak cocok",Toast.LENGTH_LONG).show();
                clearForm();
            }
        }catch (Exception e){
            Log.e("get edit text",e.getMessage());
        }
    }
    private void clearForm(){
       inEmail.setText("");
       inPass.setText("");
    }
}
